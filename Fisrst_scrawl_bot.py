from urllib import request
from parsel import Selector
from math import ceil, floor
import datetime

def parse_shop_data():
    pass

def parse_request(string):
    return string.replace(" ", '+')

def get_shop_name(page_num, etsy_request):

    shopnames_set = set()
    for page in range(1, page_num + 1):
        res = request.urlopen('https://www.etsy.com/search?q=' + etsy_request + '&explicit=1&page=' + str(page))
        content = res.read().decode('utf-8')
        sel = Selector(text=content)
        for shop_name in sel.css('p.text-gray-lighter.text-body-smaller.display-inline-block.mr-xs-1::text').extract():
            shopnames_set.add(shop_name)
    return list(shopnames_set)

def get_shop_info(shop_list):

    etsy_shops = []
    left = 0
    for shop_name in shop_list:
        try:
            res = request.urlopen("https://www.etsy.com/shop/" + shop_name)
            content = res.read().decode('utf-8')
            sel = Selector(text=content)
            sales = sel.css('span.shop-sales.hide-border.no-wrap::text').re(r'^(\d*)\s*Sale[s]?$')
            year = sel.css('span.etsy-since.no-wrap::text').re(r'On Etsy since\s*(.*)')[0]
            shop_items = sel.css('div > ul:first-child > li > a > span.badge.badge-transparent::text').extract_first()
            #print(year)
        except:
            print(shop_name)
            left += 1
            print('left' + str(left))
            continue
        if len(sales) == 0:
            sales = sel.css('span.shop-sales.hide-border.no-wrap > a.text-gray-lighter::text').re(r'^(\d*)\s*Sale[s]?')

        try:
            sales = sales[0]
            #print(shop_name + " " + sales + " " + str(year))
            etsy_shops.append(EtsyShop(int(year), int(sales), [], shop_name, shop_items))
        except:
            print(sales)
            print(shop_name)
            print(year)
    print(len(etsy_shops))
    return etsy_shops


class EtsyShop():

    def __init__(self, founded, sales, list_num, name, items):
        self.founded = founded
        self.sales = int(sales)
        self.list_num = list_num
        self.name = name
        self.shop_items = items
        self.parse_date = datetime.datetime.now()
        self.months = self.calc_months()
        self.average_sales_in_month = round(self.sales / self.months, 4)

    def calc_months(self):

        year_founded = self.founded
        parse_date = self.parse_date

        datetime_founded = datetime.datetime(year_founded, 6, 1)
        if year_founded == parse_date.year:
            return int(parse_date.month)
        else:
            return int(((parse_date - datetime_founded) / 30.4375).days)

    def avg(self):
        return self.average_sales_in_month


class Statistic():

    def __init__(self, shop_list, request_name):
        self.avarage_sales = self.calc_avg_sales_in_month(shop_list)
        self.median = self.calc_median(shop_list)
        self.arithmetic_mean = self.calc_arithmetic_mean(shop_list)
        self.varience = self.calc_varience(shop_list)
        self.standard_deviation = self.calc_standard_deviation()
        self.name = request_name
        self.total_shops = len(shop_list)
        #In this place better to create new top 10 shops instead linking on existing
        self.top_10 = shop_list[-10:]

    def calc_avg_sales_in_month(self, shop_list):
        tmp_avg = 0
        for i in shop_list:
            tmp_avg += i.average_sales_in_month
        return round(tmp_avg, 4)

    def calc_median(self, inf_list):
        if len(inf_list) % 2 == 0:
            indx1 = len(inf_list) // 2
            indx2 = indx1 - 1
            return (inf_list[indx1].avg() + inf_list[indx2].avg()) / 2
        else:
            indx = len(inf_list) // 2
            return inf_list[indx].avg()

    def calc_arithmetic_mean(self, inf_list):
        return round(self.avarage_sales / len(inf_list), 4)

    def calc_varience(self, shop_list):
        varience = 0
        for i in shop_list:
            varience += (self.arithmetic_mean - i.avg()) ** 2
        return round(varience/len(shop_list), 4)

    def calc_standard_deviation(self):
        return round(self.varience ** 0.5, 4)

    def save(self):
        filename = self.name + '.txt'
        with open(filename, "w+") as f:
            f.write("Sales in month on " + self.name + " request - " + str(self.avarage_sales) + '\n')
            f.write("Medain - " + str(self.median) +'\n')
            f.write("Arithmetic mean - " + str(self.arithmetic_mean)+'\n')
            f.write("Varience - " + str(self.varience)+'\n')
            f.write("Standard deviation - " + str(self.standard_deviation)+'\n')
            f.write("Total shops - " + str(self.total_shops)+'\n')

            for shop in self.top_10:
                f.write(shop.name + " sales in month-" + str(shop.avg()) + " All sales-" + str(shop.sales) + " Founded-" + str(
                    shop.founded) +
                      " Amount of items-" + str(shop.shop_items)+'\n')

    def __call__(self, *args, **kwargs):
        print("Sales in month on " + self.name + " request - " + str(self.avarage_sales))
        print("Medain - " + str(self.median))
        print("Arithmetic mean - " + str(self.arithmetic_mean))
        print("Varience - " + str(self.varience))
        print("Standard deviation - " + str(self.standard_deviation))
        print("Total shops - " + str(self.total_shops))

def get_pages_num(etsy_request):
    res = request.urlopen('https://www.etsy.com/search?q=' + etsy_request + '&explicit=1&page=1')
    content = res.read().decode('utf-8')
    sel = Selector(text=content)

    #text = sel.css('a.btn.btn-secondary.btn-group-item-lg.hide-xs.hide-sm.hide-md > span[aria-hidden*=true]::text').re(r'\s*(\d*)\s*')
    text = sel.css('a.btn.btn-secondary.btn-group-item-lg.hide-xs.hide-sm.hide-md::attr(data-page)').extract()
    print(text)
    text = [int(i) for i in text if i != '']
    print(text)
    return max(text)

KEY_WORD = ''

ETSY_REQUEST = parse_request(KEY_WORD)
PAGE_NUM = get_pages_num(ETSY_REQUEST)
#For tests
#PAGE_NUM = 1
print(PAGE_NUM)
Shop_names = get_shop_name(PAGE_NUM, ETSY_REQUEST)
shop_inf_list = get_shop_info(Shop_names)
shop_inf_list.sort(key=lambda x: x.average_sales_in_month)

for i in shop_inf_list:
    print(i.name + " sales in month-" + str(i.avg()) + " All sales-" + str(i.sales) + " Founded-" + str(i.founded)+
          " Amount of items-" + str(i.shop_items))

newsome = Statistic(shop_inf_list, KEY_WORD)

newsome()

newsome.save()























